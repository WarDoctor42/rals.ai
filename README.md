# Rals.ai

A discord bot written in Python3 (using the discord.py) library all centered around our favorite DELTARUNE character, Ralsei!

## Getting Started
**NOTE:** Rals.ai is still early in development and nowhere near finished. You will need to jump through some hoops in order to get him to work!

### Prerequisites

#### Dependencies
```
python >=3.5
```

```
discord.py <=0.16.0
```
#### Config
##### Tokenfile
By default, Rals.ai looks for a token string in a text file at /etc/rals.ai/tokenfile. You will need to add a bot's token here, and if you are running on Windows, you will need to change the path to something else (ex. C:\rals.ai\tokenfile.txt). **NOTE:** The token string needs to be completely by itself but a newline at the end won't affect it

## Versioning
Rals.ai uses [Semantic Versioning](https://semver.org/)

## Authors
See AUTHORS

## Contributing
See CONTRIBUTING.md

## License
See LICENSE

## Code of Conduct
See CODE_OF_CONDUCT.md
